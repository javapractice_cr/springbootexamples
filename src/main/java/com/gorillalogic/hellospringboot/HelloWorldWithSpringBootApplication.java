package com.gorillalogic.hellospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot app.
 */
@SpringBootApplication
public class HelloWorldWithSpringBootApplication {

  /**
   * Main Method.
   *
   * @param args Args
   */
  public static void main(String[] args) {
    SpringApplication.run(HelloWorldWithSpringBootApplication.class, args);
  }

}
