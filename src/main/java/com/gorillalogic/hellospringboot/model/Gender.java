package com.gorillalogic.hellospringboot.model;

/**
 * Gender enum.
 */
public enum Gender {
  MALE,
  FEMALE,
  NON_BINARY,
  INTERSEX,
  TRANS,
  NON_CONFORMING,
  NOT_DISCLOSED,
  NONE_OF_ABOVE
}
