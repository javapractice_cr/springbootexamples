package com.gorillalogic.hellospringboot.controller;

import com.gorillalogic.hellospringboot.model.Person;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world controller.
 */
@RestController
@RequestMapping("/")
public class HelloController {

  /**
   * Base url get api.
   *
   * @return Response
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Person> getBase(@RequestParam String name, @RequestParam String lastName) {
    Person person = new Person();
    person.setName(name);
    person.setLastName(lastName);
    return ResponseEntity.ok(person);
  }

  /**
   * Hello Api.
   *
   * @return Response
   */
  @GetMapping("hello")
  public String get() {
    return "Hello Controller";
  }
}
