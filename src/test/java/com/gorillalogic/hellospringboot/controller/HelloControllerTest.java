package com.gorillalogic.hellospringboot.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;


import com.gorillalogic.hellospringboot.model.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration()
public class HelloControllerTest {
  @InjectMocks
  private HelloController helloController;

  @Test
  public void sayHello(){
    ResponseEntity<Person> response = helloController.getBase("TestName", "TestLastName");
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }
}
